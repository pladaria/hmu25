document.addEventListener('DOMContentLoaded', function () {

    var tuentiApi = TuentiApi();

    var inputUsername = document.getElementById('username'),
        inputPassword = document.getElementById('password'),
        btnLogin = document.getElementById('btn-login');
        btnLogout = document.getElementById('btn-logout');
        blockLogin = document.getElementById('login'),
        blockMain = document.getElementById('main'),
        imgAvatar = document.getElementById('avatar'),
        textUserName = document.getElementById('user-name'),
        username = localStorage.getItem('user'),
        password = localStorage.getItem('pass');
        userId = localStorage.getItem('userId');
        name = localStorage.getItem('name');
        avatar = localStorage.getItem('avatar');

    var renderNameAndAvatar = function () {
        imgAvatar.src = avatar;
        textUserName.innerHTML = name;
    }

    if (!username || !password) {
        blockLogin.className = '';
    } else {
        blockMain.className = '';
        renderNameAndAvatar();
        tuentiApi.setUsername(username);
        tuentiApi.setPassword(password);
    }

    inputUsername.focus();

    btnLogout.onclick = function () {
        inputUsername.value = '';
        inputPassword.value = '';
        username = null;
        password = null;
        localStorage.clear('user');
        localStorage.clear('pass');
        localStorage.clear('userId');
        localStorage.clear('avatar');
        localStorage.clear('name');
        blockLogin.className = '';
        blockMain.className = 'hide';
        console.log('logout');
    }

    btnLogin.onclick = function (e) {
        e.preventDefault();
        username = inputUsername.value.trim();
        password = inputPassword.value.trim();
        if (username && password) {
            password = MD5.digest(password);
            tuentiApi.setUsername(username);
            tuentiApi.setPassword(password);

            tuentiApi.call('Auth', 'getSessionInfo', {}, {}, function (err, data) {
                if (err) {
                    return inputUsername.focus();
                }
                data = data[0];
                userId = String(data.userId);
                var cdn = data.servers.publicPhotos;
                tuentiApi.call('User', 'getUsersData', {ids: [userId]}, {}, function (err, data) {
                    if (err) {
                        return inputUsername.focus();
                    }
                    data = data[0].users[0];
                    avatar = cdn + data.avatar.hash;
                    name = data.name + ' ' + data.surname[0] + '.';
                    localStorage.setItem('userId', userId);
                    localStorage.setItem('avatar', avatar);
                    localStorage.setItem('name', name);
                    localStorage.setItem('user', username);
                    localStorage.setItem('pass', password);
                    renderNameAndAvatar();
                    blockLogin.className = 'hide';
                    blockMain.className = '';
                });
            });

        } else {
            if (!username) {
                inputUsername.focus();
            } else {
                inputPassword.focus();
            }
        }

        return false;
    };

}, false);
