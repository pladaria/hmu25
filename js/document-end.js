
chrome.runtime.sendMessage({msg: 'isActive?'}, function(response) {
    console.log('isActive?', response.msg);
    var active = !!response.msg;

    if (active) {
        document.addEventListener('click', function (e) {
            if (e.target.___number) {
                chrome.runtime.sendMessage({msg: 'call:' + e.target.___number}, function(res) {
                    console.log(res);
                });
            }
        }, false);

        document.addEventListener('mouseover', function (e) {
            inspectNode(e.target);
        }, false);

        document.addEventListener('mouseout', function (e) {
            restoreNode(e.target);
        }, false);
    }

});

var inspectNode = function (node) {
    var children = node.childNodes;
    Array.prototype.forEach.bind(children)(function (child) {
        if (child.nodeType == 3) {
            var text = child.nodeValue.trim();
            if (text.match(/\+?[\d -]{9,15}/)) {
                if (parent.___hover) return;
                digits = text.replace(/[^\d+]/g, '');
                parent = child.parentNode;
                parent.___hover = true;
                parent.___number = digits;
                parent.___oldCursor = parent.style.cursor;
                parent.___oldColor = parent.style.color;
                parent.___oldtextShadow = parent.style.textShadow;
                parent.style.cursor = 'pointer';
                parent.style.color = 'rgb(0, 113, 205)';
                parent.style.textShadow = '2px 2px 2px #fff';
            }
        }
    });
};

var restoreNode = function (node) {
    if (node.___hover) {
        node.style.cursor = node.___oldCursor;
        node.style.color = node.___oldColor;
        node.style.textShadow = node.___oldtextShadow;
        node.___hover = false;
    }
};
