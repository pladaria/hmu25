var TuentiApi = function () {

    var username = '',
        password = '';

    var setHeaders = function (req, headers) {
        Object.keys(headers).forEach(function (header) {
            req.setRequestHeader(header, headers[header]);
        });
    };

    var getPostData = function (api, method, params) {
        return JSON.stringify({
            version: 'msngr-3',
            requests: [[api + '_' + method, params]],
            screen: 'xhdpi',
        });
    };

    var call = function (api, method, params, headers, callback) {
        var req = new XMLHttpRequest();

        headers = headers || {};
        headers['X-Tuenti-Authentication'] =
            'user=' + username + ',' +
            'password=' + password + ',' +
            'installation-id=dummy,' +
            'device-family=MDI3MDFmZjU4MGExNWM0YmEyYjA5MzRkODlmMjg0MTU6MC4yMjk5ODcwMCAxMzI0NDg5NjY0';

        req.onload = function () {
            var data = req.responseText;
            try {
                data = JSON.parse(data);
            } catch (ignore) {
            }
            if (req.status == 200) {
                callback(null, data);
            } else {
                callback(req.status, data);
            }
        };

        req.onerror = function (err) {
            console.log('err', err);
        };

        req.open('post', 'https://alpha-contacts-pub-7.tuenti.com/index.msngr.php', true);

        setHeaders(req, headers);

        req.send(getPostData(api, method, params));
    };

    return {
        setUsername: function(value) {
            username = value;
        },
        setPassword: function(value) {
            password = value;
        },
        call: call
    };
};
