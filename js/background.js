var tuentiApi = TuentiApi(),
    NOTIFICATION_ID = 'tuenti-extension-' + new Date().getTime();

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

    //console.log(sender.tab ? "from a content script:" + sender.tab.url : "from the extension");

    if (request.msg == 'isActive?') {
        var result = !!localStorage.getItem('user');
        return sendResponse({msg: result});
    }
    if (request.msg.match(/^call:/)) {
        tuentiApi.setUsername(localStorage.getItem('user'));
        tuentiApi.setPassword(localStorage.getItem('pass'));
        var number = request.msg.replace(/[^\d^+]+/, '');

        console.log('call:', number);

        tuentiApi.call('Voip', 'notifyDialPhone', {phone: number}, {}, function (err, data) {
            if (err) {
                return console.log('err', data);
            }
            return console.log('ok', data);
        });

        chrome.notifications.create(
            NOTIFICATION_ID,
            {
                title: 'Dialing...',
                message: 'Phone number: ' + number,
                type: 'basic',
                iconUrl: 'res/notification.png'
            },
            function () {
                setTimeout(function () {
                    chrome.notifications.clear(NOTIFICATION_ID, function () {});
                }, 5000);
                return NOTIFICATION_ID;
            }
        );

        return sendResponse({msg: 'dialing'});
    }
});

